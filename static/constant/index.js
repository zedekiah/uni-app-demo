export const URL = "http://127.0.0.1:1110"; // node静态访问地址
export const LEEY_MUSIC_TOKEN = "leey_music_token" // 本地存放token的key名
export const LEEY_MUSIC_USER = "leey_music_user" // 本地存放user信息的key名
export const LEEY_MUSIC_CURRENT_PAGE = "leey_music_current_page" // 存放当前的tabber page名
export const LEEY_MUSIC_HISTORY_SEARCH = 'leey_music_history_search'
