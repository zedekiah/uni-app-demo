const state = {
	online: 0, // 在线人数
	records: [], // 聊天记录
}

const mutations = {
	setOnline(state, count) {
		state.online = count
	},
	setRecords(state, records) {
		state.records = records
	},
	pushRecords(state, record) {
		state.records.push(record)
	}
}

const getters = {
	getOnline(state) {
		return state.online
	},
	getRecords(state) {
		state.records.sort((a, b) => new Date(a.createTime) - new Date(b.createTime))
		return state.records
	},
}

export default {
	namespaced: true,
	state,
	mutations,
	getters
}
