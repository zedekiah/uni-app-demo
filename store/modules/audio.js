import {
	getSongInfo,
	getLyric
} from '@/coderwhy_api/request/player/index.js';
import {
	parseLyric
} from "@/utils/format-lyric.js"
import {
	playNextSongByMode,
	playPreOrNextSong
} from "@/utils/next-song.js"
const modeArr = [{
		label: '单曲循环',
		value: 0
	},
	{
		label: '随机播放',
		value: 1
	},
	{
		label: '顺序播放',
		value: 2
	}
]
const audioCtx = uni.createInnerAudioContext();
const state = {
	audioCtx, // 播放器上下文
	info: null, // 歌曲信息
	id: null, // 当前播放的歌曲id
	picUrl: '', // cd图片
	lyric: "", // 歌词
	lyricProvider: null, // 歌词提供者信息
	currentLyricIndex: 0, // 当前歌词的索引
	currentScrollTop: 0, // 当前滚动距离
	sliderValue: 0, // 滑块
	currentTime: 0, // 当前播放的时长
	isPlay: false, // 是否正在播放
	isSlider: false, // 是否正在拖拽进度条
	playList: [], // 播放列表
	mode: 0,
}

const mutations = {
	// 播放歌曲
	play(state, payload, rootState) {
		console.log('this', this);
		const {
			audioCtx,
			info,
			id
		} = state // 获取参数
		this.commit('audio/reset') // 重置
		audioCtx.stop() // 先停止上一次播放的歌曲
		audioCtx.src = `https://music.163.com/song/media/outer/url?id=${id}.mp3`
		audioCtx.autoplay = true
		// 添加到播放list
		const {
			name,
			ar,
			id: currentId
		} = info
		const isExsit = state.playList.find(item => item.id === currentId)
		if (!isExsit) {
			const singers = ar && ar.map(item => item.name).join('/')
			// for (let i = 0; i < 10; i++) {
			state.playList.push({
				id: currentId,
				name,
				singers
			})
			// }
		}
		audioCtx.onCanplay(() => {
			console.log('可以播放了');
			state.isPlay = true
			audioCtx.play()
		})
		// 播放中
		audioCtx.onTimeUpdate(() => {
			// 1.获取当前时间
			const currentTime = audioCtx.currentTime * 1000
			// 2.修改currentTime
			state.currentTime = currentTime
			// 3.修改滑块value
			if (!state.isSlider) {
				state.sliderValue = currentTime / info.dt * 100
			}
			// 4.匹配当前的歌词
			if (!state.lyric) return;
			let i = 0
			for (; i < state.lyric.length; i++) {
				const currentLyric = state.lyric[i]
				if (currentTime < currentLyric.time) {
					break;
				}
			}
			// 
			const currentIndex = i - 1
			if (state.currentLyricIndex !== currentIndex) {
				const currentLyric = state.lyric[currentIndex]
				state.currentLyricIndex = currentIndex // 当前的index
				state.currentScrollTop = currentIndex * 35
				console.log(currentLyric.text);
			}
		})

		// 自然播放结束了
		audioCtx.onEnded(() => {
			const nextId = playNextSongByMode(state.mode, state.id, state.playList)
			console.log('当前歌曲自然播放结束, 下一首', nextId);
			this.dispatch('audio/_getSongInfo', nextId)
		})

		// 播放失败
		audioCtx.onError((e) => {
			console.log('播放失败了', e);
			state.isPlay = false
			state.sliderValue = 0
			state.currentTime = 0
			state.currentScrollTop = 0
			state.currentLyricIndex = 0
		})
	},
	// 重置
	reset(state) {
		console.log("重置")
		state.isPlay = false
		state.sliderValue = 0
		state.currentTime = 0
		state.currentScrollTop = 0
		state.currentLyricIndex = 0
	},
	// 设置currentTime
	setCurrentTime(state, t) {
		state.isSlider = false
		const {
			audioCtx
		} = state
		console.log('t', t);
		state.currentTime = t
		audioCtx.pause() // 先暂停
		const seekTime = state.currentTime / 1000
		audioCtx.seek(seekTime) // 跳到指定进度
		audioCtx.play()
	},
	// 表示正在拖动进度条中
	toggleIsSlider(state) {
		state.isSlider = true
	},
	// 暂停/播放
	togglePlay(state) {
		const {
			audioCtx
		} = state
		if (state.isPlay) {
			state.isPlay = false
			audioCtx.pause()
		} else {
			state.isPlay = true
			audioCtx.play()
		}
	},
	// 修改播放模式
	changeMode(state) {
		const {
			mode
		} = state
		if (mode === 2) {
			state.mode = 0;
		} else {
			state.mode += 1;
		}
		uni.$u.toast(modeArr[state.mode].label)
	},
	// 上一首 下一首
	preOrNext(state, t) {
		const nextId = playPreOrNextSong(t, state.id, state.playList)
		this.dispatch('audio/_getSongInfo', nextId)
	},
	// 删除播放列表中的某首歌
	deleteSong(state, id) {
		state.playList = state.playList.filter(item => item.id !== id)
		if (state.playList.length === 0) {
			// 删完了, 返回至首页
			state.audioCtx.stop()
			this.commit('audio/reset') // 重置
			uni.switchTab({
				url: '/pages/index/index'
			});
		} else if (id === state.info.id) {
			state.audioCtx.stop()
			// 删的是正在播放的
			let nextId
			if (state.mode === 0) {
				// 单曲循环时 直接跳到第一首
				nextId = state.playList[0].id
			} else {
				nextId = playNextSongByMode(state.mode, state.id, state.playList)
			}
			this.dispatch('audio/_getSongInfo', nextId)
		}
	},
	// 删除整个列表
	deletePlayList() {
		state.playList = []
		// 返回至首页
		uni.switchTab({
			url: '/pages/index/index'
		});
	}
}

const actions = {
	// 获取歌曲信息 + 歌词信息
	async _getSongInfo(ctx, id) {
		console.log('调用', id);
		const {
			commit,
			state
		} = ctx
		const {
			songs
		} = await getSongInfo({
			ids: id
		});
		if (songs[0]) {
			state.info = songs[0]
			state.id = songs[0].id
			state.picUrl = songs[0].al && songs[0].al.picUrl
		}
		// 歌词
		const {
			lyricUser,
			lrc
		} = await getLyric({
			id
		});
		state.lyricProvider = lyricUser || null
		const lyricString = (lrc && lrc.lyric) || ''
		if (lyricString) {
			state.lyric = parseLyric(lyricString)
		}
		// 调用播放
		commit('play')
	}
}


export default {
	namespaced: true,
	state,
	actions,
	mutations
}
