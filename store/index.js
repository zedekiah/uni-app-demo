import Vue from 'vue'
import Vuex from 'vuex'
import {
	LEEY_MUSIC_CURRENT_PAGE,
	LEEY_MUSIC_USER
} from '@/static/constant/index.js'
import {
	_getUserInfo
} from "@/api/request/auth/index.js"

Vue.use(Vuex)
import audio from "./modules/audio"
import soket from "./modules/soket"
const store = new Vuex.Store({
	state: {
		statusBarHeight: 0,
		isShowMenu: false,
		tabName: "index",
		user: {}

	},
	mutations: {
		// 获取屏幕状态栏的高度
		setStatusBarHeight(state, h) {
			state.statusBarHeight = h
		},
		// 切换个人菜单显示
		toggleShowMenu(state, t) {
			state.isShowMenu = !state.isShowMenu
		},
		// tabber切换
		toggleTabbar(state, name) {
			state.tabName = name
			uni.setStorageSync(LEEY_MUSIC_CURRENT_PAGE, name) // 避免刷新后丢失记录
		},
		// 存放user信息
		setUser(state, user) {
			state.user = user
			uni.setStorageSync(LEEY_MUSIC_USER, user);
		}
	},
	actions: {
		async _getUser(ctx, payload = {}) {
			if (!payload.id) return
			const res = await _getUserInfo(payload)
			if (!res.data) {
				uni.$u.toast("没有用户信息")
				return
			} else {
				ctx.commit('setUser', res.data)
			}
		}
	},
	modules: {
		audio,
		soket
	}
})

export default store
