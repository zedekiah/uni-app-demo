import {
	LEEY_MUSIC_TOKEN
} from "../static/constant/index"
/**
 * 
 * @param {请求地址} url 
 * @param {方法} method 
 * @param {参数} data 
 * @param {其他的配置项: 比如上传文件时需要修改responseType} config 
 */
export default (url, method, data, config) => {
	return new Promise((resolve, reject) => {
		// coderwhy没有登录校验 这边手动操作一下
		const token = uni.getStorageSync(LEEY_MUSIC_TOKEN)
		// const baseURL = process.env.NODE_ENV === 'development' ? '/lee' : "https://coderwhy-music.vercel.app/"
		let baseURL = "https://coderwhy-music.vercel.app/"
		// #ifdef H5
		baseURL = "/lee"
		// #endif
		uni.request({
			url: baseURL + url,
			method,
			data,
			header: {
				'lee-authorize': token //自定义请求头信息
			},
			success: (res) => {
				if (res.statusCode !== 200) {
					uni.$u.toast(res.errMsg)
					reject(res)
				} else {
					console.log(res);
					resolve(res.data)
				}
			},
			fail: (err) => {
				console.log('触发fail', err, );
				uni.$u.toast(err)
				reject(err)
			},
			...config
		});
	})
}
