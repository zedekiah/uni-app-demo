import request from "../../coderwhy_index.js"

export const getBanner = (params) => {
	return request("/banner", 'GET', params)
}
