import request from "../../coderwhy_index.js"

// 获取热搜列表(简略)
export const getHotSearchList = () => request('/search/hot')
// 获取热搜列表(详细)
export const getHotSearchDetailList = () => request('/search/hot/detail')
// 热门dj
export const getHotDjList = (params) => request('/dj/hot', "GET", params)
// 热门歌单
export const getHotSongSheet = () => request('/playlist/hot')
// 搜索建议
export const getSearchSuggest = (params) => request("/search/suggest", "GET", params)
// 搜索结果
export const getSearchResult = (params) => request('/search', 'GET', params)
// 获取推荐新歌曲
export const getNewSongs = () => request('/personalized/newsong')
// 检测音乐是否可用
export const checkMusic = (params) => request('/check/music', 'GET', params)
// 获取歌曲信息
export const getSongInfo = (params) => request('/song/detail', 'GET', params)
