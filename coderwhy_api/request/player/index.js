import request from "../../coderwhy_index.js";
// 获取歌曲信息
export const getSongInfo = (params) => request('/song/detail', 'GET', params)
// 外链播放
// https://music.163.com/song/media/outer/url?id=id.mp3
// 获取歌词
export const getLyric = (params) => request('/lyric', 'GET', params)
