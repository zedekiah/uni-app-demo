const words = ['草', '操', '肏', '妈']

export const peaceAndLove = (msg) => {
	if (!msg) return ''
	let copyMsg = ''
	for (let i = 0; i < msg.length; i++) {
		if (words.includes(msg[i])) {
			copyMsg += '*'
		} else {
			copyMsg += msg[i]
		}
	}
	return copyMsg
}
