import store from "@/store"
let socketTask = ''
let is_open_socket = false //避免重复连接
let data = null
//心跳检测
let timeout = 5000 //多少秒执行检测
let heartbeatInterval = null //检测服务器端是否还活着
let reconnectTimeOut = null //重连之后多久再次重连
let closeFlag = true //退出登录的手动关闭
// 进入这个页面的时候创建websocket连接【整个页面随时使用】
export function connectSocketInit(id) {
	let online = 0
	let query = {
		token: uni.getStorageSync('leey_music_token'),
		id
	}
	socketTask = uni.connectSocket({
		url: `ws://127.0.0.1:1110/lee/ws${uni.$u.queryParams(query)}`,
		header: {
			'content-type': 'application/json'
		},
		success: () => {
			console.log("正准备建立websocket中...");
			// 返回实例
			return socketTask
		}
	})

	socketTask.onOpen((res) => {
		console.log("WebSocket连接正常！");
		clearTimeout(reconnectTimeOut)
		clearInterval(heartbeatInterval)
		is_open_socket = true;
		// start(); // 开发阶段关闭心跳检测
		// 注：只有连接正常打开中 ，才能正常收到消息
		socketTask.onMessage(({
			data
		}) => {
			const _data = JSON.parse(data)
			const {
				code,
				message
			} = _data
			if (code === 1110) {
				// 心跳检测
			} else if (code === 1111) {
				console.log('有新人加入->new', _data);
				// 设置在线人数
				store.commit('soket/setOnline', _data.online)
				// 如果当前用户是新人 把之前的聊天记录获取一份
				id === ~~_data.id && store.commit('soket/setRecords', _data.records)
			} else if (code === 0) {
				console.log('有人离开了', _data);
				// 设置在线人数
				store.commit('soket/setOnline', _data.online)
			} else if (code === 999) {
				console.log('有人撤回了一条消息');
				if (_data.id === id) {
					// 反馈用户本人
					uni.$u.toast('撤回成功~')
				}
				store.commit('soket/setRecords', _data.records)
			} else {
				console.log('收到来自服务端的消息->talk', _data);
				store.commit('soket/pushRecords', _data)
			}
		});
	})
	socketTask.onClose(() => {
		if (closeFlag) {
			console.log("已经被关闭了")
			is_open_socket = false;
			reconnect();
		} else {
			closeFlag = true
		}

	})
}

//发送消息
export function send(value) {
	console.log('触发send函数', value);
	return new Promise((resolve, reject) => {
		if (socketTask !== null && socketTask.readyState === 3) {
			socketTask.close()
			reconnect() //重连
		} else if (socketTask.readyState === 1) {
			if (value.type !== 'ping') {
				console.log('发送的数据', value);
			}
			socketTask.send({
				data: JSON.stringify(value),
				success: resolve
			});
		} else if (socketTask.readyState === 0) {
			setTimeout(() => {
				socketTask.send({
					data: JSON.stringify(value),
					success: resolve
				});
			}, timeout)
		}
	})
}
//开启心跳检测
function start() {
	heartbeatInterval = setInterval(() => {
		// console.log(let data)
		send({
			type: 'ping'
		});
	}, timeout)
}
//重新连接
function reconnect() {
	//停止发送心跳
	clearInterval(heartbeatInterval)
	//如果不是人为关闭的话，进行重连s
	if (!is_open_socket) {
		reconnectTimeOut = setTimeout(() => {
			connectSocketInit();
		}, 5000)
	}
}
// 关闭长链接
export function removeWebsocket(id) {
	clearInterval(heartbeatInterval)
	clearTimeout(reconnectTimeOut)
	socketTask.close()
	socketTask = ''
	is_open_socket = false
	data = null
	timeout = 5000
	heartbeatInterval = null
	reconnectTimeOut = null
	closeFlag = false
}
