export default function(selector, _) {
	return new Promise(resolve => {
		const query = uni.createSelectorQuery().in(_);
		query
			.select(selector)
			.boundingClientRect(data => {
				resolve(data.height)
			})
			.exec();
	})
}
