import dayjs from 'dayjs'
// 12 -> 12
// 5 -> 05
export const padLeftZero = (time) => {
	time = time + ""
	return ("00" + time).slice(time.length)
}

export const formatDuration = (duration) => {
	duration = duration / 1000
	// 488s / 60 = 8.12
	var minute = Math.floor(duration / 60)
	// 488s % 60
	var second = Math.floor(duration) % 60
	return padLeftZero(minute) + ":" + padLeftZero(second)
}


export const dateFormat = (date, type = "YYYY/MM/DD HH:mm:ss") => {
	if (!date) return "";
	return dayjs(date).format(type);
};
