import emoji from "@/js_sdk/m-emoji/m-emoji_2.0.0/emoji.js";
const page = Math.ceil(emoji.length / 21);
const data = []
for (let i = 0; i < page; i++) {
	data[i] = [];
	for (let k = 0; k < 21; k++) {
		emoji[i * 21 + k] ? data[i].push(
			emoji[i * 21 + k]
		) : ''
	}
}

export default data
