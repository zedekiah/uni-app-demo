// 音乐播放 计算下一首歌曲
export const playNextSongByMode = (mode, id, list) => {
	// 0: 单曲 1:随机 2:顺序
	if (mode === 0) return id
	if (mode === 1) {
		const random =
			Math.floor(Math.random() * (list.length - 1 + 1) + 1)
		return list[random - 1].id
	}
	if (mode === 2) {
		let index = list.findIndex(item => ~~item.id === ~~id)
		index = index === list.length - 1 ? 0 : ++index
		return list[index].id
	}
}

// 上一首下一首(不受mode影响)
export const playPreOrNextSong = (t, id, list) => {
	if (list.length === 1) return list[0].id
	// 获取当前
	let index = list.findIndex(item => item.id === id)
	if (t) {
		// 下一首
		index = index === list.length - 1 ? 0 : ++index
	} else {
		// 上一首
		index = index === 0 ? list.length - 1 : --index
	}
	return list[index].id
}
