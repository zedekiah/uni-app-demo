import App from './App'
// #ifndef VUE3
import Vue from 'vue'
// u-view
import uView from '@/uni_modules/uview-ui'
import store from '@/store'
Vue.use(uView)
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App,
	store
})
app.$mount()
// #endif

//main.js
// #ifdef MP
	Vue.mixin({
	  onLoad: function () {
		wx.hideShareMenu();
	  }
	})
// #endif


// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif