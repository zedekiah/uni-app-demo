import {
	_get,
	_post
} from "../index.js"

// 获取聊天背景图
export const _getChatBg = () => _get("/chat-bg")

// 更新个人聊天背景
export const _updateChatBg = (data) => _post("/chat-bg-update", data)
