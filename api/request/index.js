import Service from "../axios"
import config from "../config.js"
import {
	LEEY_MUSIC_TOKEN
} from "../../static/constant/index.js"
const token = uni.getStorageSync(LEEY_MUSIC_TOKEN) || ""
const instance = new Service(config).instance
instance.defaults.headers["lee-authorize"] = token || "";

// 自定义适配器
instance.defaults.adapter = function(config) {
	return new Promise((resolve, reject) => {
		var settle = require('axios/lib/core/settle');
		var buildURL = require('axios/lib/helpers/buildURL');
		uni.request({
			method: config.method.toUpperCase(),
			url: config.baseURL + buildURL(config.url, config.params, config.paramsSerializer),
			header: config.headers,
			data: config.data,
			dataType: config.dataType,
			responseType: config.responseType,
			sslVerify: config.sslVerify,
			complete: function complete(response) {
				// console.log("执行完成：", response)
				response = {
					data: response.data,
					status: response.statusCode,
					errMsg: response.errMsg,
					header: response.header,
					config: config
				};
				settle(resolve, reject, response);
			}
		})
	})
}

export const _get = (url, params) => {
	return instance.get(url, {
		params
	})
}

export const _post = (url, data) => {
	return instance.post(url, {
		data
	})
}
