import {
	_get,
	_post
} from "../index"
// 获取验证码
export const getVerificationCode = (params) => _get("/leey-music/verification-code", params)
// 登录
export const login = (data) => _post("/leey-music/login", data)

// 获取当前用户信息
export const _getUserInfo = (params) => _get("/leey-music/user-info", params)
