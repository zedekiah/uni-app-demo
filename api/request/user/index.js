import {
	_get,
	_post
} from "../index"

export const login = (data) => _post("/login", data)
