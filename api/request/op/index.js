import { _get, _post } from "../index.js"

export const getOPbanner = (params) => {
	return _get("/op/banner", params)
}