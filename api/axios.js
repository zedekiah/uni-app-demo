import axios from "axios";
class Service {
	instance;
	interceptors;
	constructor(config) {
		// 构建实例
		this.instance = axios.create(config)
		// 每个实例各自的拦截器(特别情况才会用到)
		this.interceptors = config.interceptors
		// 个体的请求拦截
		this.instance.interceptors.request.use(
			this.interceptors && this.interceptors.requestInterceptor,
			this.interceptors && this.interceptors.requestInterceptorCatch
		)
		// 个体的响应拦截
		this.instance.interceptors.response.use(
			this.interceptors && this.interceptors.responseInterceptor,
			this.interceptors && this.interceptors.responseInterceptorCatch
		)

		// 共有的拦截器
		this.instance.interceptors.request.use(
			(config) => config,
			(err) => Promise.reject(err))

		this.instance.interceptors.response.use(
			(res) => {
				return res.data;
			},
			(err) => {
				console.log("err", err);
				uni.$u.toast(err)
				return Promise.reject(err);
			})
	}
}

export default Service
